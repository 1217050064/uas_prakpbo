# NO.1
Use Case User (Pengguna):
Membuat akun pengguna untuk mengakses konten Disney+ Hotstar.
Menjelajahi koleksi film dan serial TV Disney, Pixar, Marvel, Star Wars, dan National Geographic.
Mencari dan menonton film Bollywood terbaru dan klasik.
Menonton konten dalam berbagai bahasa, seperti Hindi, Tamil, Telugu, Malayalam, dan lainnya.
Menyimpan film dan serial TV favorit dalam daftar tontonan.
Menggunakan fitur pengunduhan untuk menonton konten secara offline.
Mengaktifkan opsi pemutaran konten dengan resolusi 4K HDR.
Menggunakan fitur rekomendasi untuk menemukan konten baru berdasarkan preferensi pengguna.
Menonton siaran langsung olahraga seperti sepak bola, kriket, dan tenis.
Mengakses konten eksklusif Disney+ Hotstar.

Use Case Manajemen Perusahaan:
Mengelola konten yang akan diunggah ke platform Disney+ Hotstar.
Menjadwalkan perilisan konten baru, termasuk film, serial TV, dan acara olahraga.
Mengelola lisensi dan hak siar untuk konten yang akan ditampilkan di Disney+ Hotstar.
Mengelola penawaran dan paket langganan untuk pelanggan Disney+ Hotstar.
Mengatur dan memantau kebijakan penggunaan dan akses konten.
Mengelola akun pengguna dan memberikan dukungan teknis kepada pengguna.
Menganalisis data pengguna dan perilaku tontonan untuk meningkatkan pengalaman pengguna.
Mengintegrasikan platform Disney+ Hotstar dengan sistem manajemen perusahaan yang ada.

Use Case Direksi Perusahaan (Dashboard, Monitoring, Analisis):
Mengakses dashboard yang memberikan gambaran tentang kinerja Disney+ Hotstar, termasuk jumlah pelanggan, tingkat retensi, dan pendapatan.
Menganalisis data pengguna dan statistik tontonan untuk memahami tren dan preferensi pengguna.
Memantau kesuksesan peluncuran konten baru dan mengevaluasi kinerja konten yang ada.
Memantau kepatuhan terhadap peraturan dan kebijakan terkait hak cipta dan lisensi.
Menganalisis data bisnis, termasuk pendapatan iklan dan penjualan langganan.
Membuat laporan dan analisis untuk keputusan strategis terkait pengembangan platform Disney+ Hotstar.
Memantau performa kompetitor dan tren industri dalam industri layanan streaming.


# NO.2

Kelas Pengguna (User): Mewakili pengguna aplikasi Disney+ Hotstar. Memiliki atribut seperti nama pengguna, alamat email, dan kata sandi. Metode dapat mencakup fungsi login, logout, dan mengelola profil pengguna.
Kelas Konten (Content): Mewakili konten yang tersedia di Disney+ Hotstar, seperti film, serial TV, atau acara olahraga. Memiliki atribut seperti judul, deskripsi, dan genre. Metode dapat mencakup fungsi mencari konten, mendapatkan informasi detail, dan menambahkan ke daftar tontonan.
Kelas Daftar Tontonan (Watchlist): Mewakili daftar konten yang ditandai oleh pengguna untuk ditonton nanti. Memiliki atribut berupa daftar objek Konten yang ditandai oleh pengguna. Metode dapat mencakup fungsi menambahkan dan menghapus konten dari daftar tontonan.
Kelas Streaming (Streaming): Mewakili layanan streaming untuk menonton konten di Disney+ Hotstar. Memiliki atribut berupa URL streaming atau media player. Metode dapat mencakup fungsi memulai pemutaran, menghentikan pemutaran, dan mengatur kualitas video.
Kelas Pencarian (Search): Mewakili fitur pencarian di aplikasi Disney+ Hotstar. Memiliki metode untuk melakukan pencarian berdasarkan kata kunci atau filter tertentu, dan mengembalikan hasil yang sesuai.
Kelas Analitik (Analytics): Mewakili komponen analitik untuk melacak dan menganalisis data pengguna, seperti preferensi konten atau statistik penggunaan. Memiliki metode untuk mengumpulkan dan menganalisis data pengguna, serta menghasilkan laporan atau wawasan yang relevan.
Kelas Sistem Pembayaran (Payment System): Mewakili sistem pembayaran untuk melakukan transaksi pembelian langganan atau konten berbayar di Disney+ Hotstar. Memiliki metode untuk mengatur proses pembayaran, memverifikasi pembayaran, dan mengirim konfirmasi transaksi.
Kelas Manajemen Konten (Content Management): Mewakili komponen manajemen konten dalam aplikasi Disney+ Hotstar. Memiliki metode untuk mengelola dan mengatur konten yang ditampilkan, termasuk proses unggah, pembaruan, dan penghapusan konten.

# NO.3

Solid design principles (Prinsip-prinsip Desain SOLID) adalah seperangkat prinsip yang digunakan untuk memandu desain perangkat lunak yang baik dan dapat dipelihara. Prinsip-prinsip ini membantu dalam membangun kode yang mudah dimengerti, terstruktur dengan baik, dan fleksibel untuk perubahan di masa depan. Berikut adalah bagaimana beberapa prinsip SOLID dapat diterapkan dalam desain aplikasi Disney+ Hotstar:

Single Responsibility Principle (Prinsip Tanggung Jawab Tunggal): Setiap kelas dalam aplikasi harus memiliki tanggung jawab tunggal dan fokus pada satu aspek dari fungsionalitas. Misalnya, ada kelas yang bertanggung jawab untuk mengelola autentikasi pengguna, kelas untuk mengelola konten, dan kelas untuk mengelola pembayaran. Dengan menerapkan prinsip ini, perubahan pada satu aspek fungsionalitas tidak akan mempengaruhi kelas lain secara langsung.

Open/Closed Principle (Prinsip Terbuka/Tertutup): Modul atau kelas dalam aplikasi harus terbuka untuk perluasan namun tertutup untuk modifikasi. Dalam konteks Disney+ Hotstar, ini berarti ketika menambahkan fitur baru, sebaiknya dilakukan dengan menambahkan kelas baru atau memperluas fungsionalitas yang ada, tanpa harus memodifikasi kelas yang sudah ada.

Liskov Substitution Principle (Prinsip Substitusi Liskov): Subkelas harus dapat digunakan sebagai pengganti kelas induk tanpa mengubah kebenaran program. Dalam aplikasi Disney+ Hotstar, ini berarti jika ada subkelas yang mewarisi kelas induk (seperti subkelas Konten), maka subkelas tersebut harus bisa digunakan dengan benar dalam konteks penggunaan yang sama seperti kelas induk.

Interface Segregation Principle (Prinsip Segregasi Interface): Klien tidak boleh dipaksa untuk mengimplementasikan metode yang tidak relevan bagi mereka. Dalam aplikasi Disney+ Hotstar, jika ada antarmuka atau kontrak yang berhubungan dengan interaksi pengguna, perlu dipastikan bahwa antarmuka tersebut tersegmentasi dengan baik dan hanya berisi metode yang relevan dan diperlukan oleh klien yang menggunakan antarmuka tersebut.

Dependency Inversion Principle (Prinsip Inversi Ketergantungan): Modul atau kelas harus bergantung pada abstraksi, bukan pada implementasi. Dalam aplikasi Disney+ Hotstar, ini berarti ketergantungan antar kelas harus tergantung pada kontrak antarmuka yang didefinisikan, bukan pada implementasi kelas secara langsung. Ini memudahkan penggantian implementasi kelas tanpa mempengaruhi kelas yang bergantung padanya.

![https://gitlab.com/1217050064/uas_prakpbo/-/raw/main/UAS3.gif]

# NO.4

Factory Pattern:
Factory pattern digunakan untuk membuat objek dengan mengabstraksi proses pembuatan objek. Dalam konteks Disney+ Hotstar, factory pattern dapat digunakan untuk membuat objek konten (film, serial TV) berdasarkan jenis atau genre tertentu.

![https://gitlab.com/1217050064/uas_prakpbo/-/raw/main/UAS4.gif]

# NO.5

Database yang digunakan adalah API dari movies yang tersedia dan untuk database usernya adalah menggunakan conslose google

![https://gitlab.com/1217050064/uas_prakpbo/-/raw/main/UAS5.gif]

![https://gitlab.com/1217050064/uas_prakpbo/-/raw/main/UAS5.1.png]

# NO.6

CRUD yang di lakukan pada aplikasi Disney Hotstar yaitu menambah user dan berhenti berlanggan pada aplikasi Disney Hotstar

![https://gitlab.com/1217050064/uas_prakpbo/-/blob/main/UAS6.gif]

# NO.7

Graphical User Interface dari Aplikasi Disney Hotstar yaitu contohnya yaitu pada menu login, tetapi untuk menu selanjutnya belum bisa ditampilkan karena terkendala Error Login yang belum bisa ditemukan

![https://gitlab.com/1217050064/uas_prakpbo/-/raw/main/UAS7.gif]

# NO.8

Dalam aplikasi Disney+ Hotstar, HTTP connection biasanya tidak terlihat secara langsung melalui antarmuka pengguna (GUI). Namun, HTTP connection digunakan secara internal oleh aplikasi untuk menghubungkan dengan server backend dan mengambil data konten yang ditampilkan kepada pengguna.

! [https://gitlab.com/1217050064/uas_prakpbo/-/raw/main/UAS8.gif]

# NO.9

![https://youtu.be/uYFLyKjn5e0]

# NO.10

![https://youtu.be/uYFLyKjn5e0]

